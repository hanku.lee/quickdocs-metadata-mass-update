# Project overview

This project contains the source content for updating document metadata of Fedora's Quick Docs repository.

## Bulk editing of documentation metadata

The goal strives for update of document metadata by appending the files with defined categories and tags for each article. Shell scripting or Python could do this job. The script can be reusable for similar tasks to update metadata.

- Metadata on AsciiDoc Document Header

:category: CATEGORY

:tags: TAG_01 TAG_02 TAG_03 ... TAG_n

- Reference: https://docs.fedoraproject.org/en-US/fedora-docs/contributing-docs/asciidoc-markup/#_document_header
- Docs repo URL: https://pagure.io/fedora-docs/quick-docs.git

## Rough process steps

- Step 1: List the files and metadata and save them onto a CSV file
Refer to the script 'learnToScript.sh' on this repository
- Step 2: Update metadata on a CSV file
- Step 3: List the files and metadata, loop over them, append the text with echo "text" >> $file

## Authors and acknowledgment

Credit to A.Sinha in the Fedora Project, who created the script and submitted MR on step 1.

I'm grateful to Chris Gioran (@chrisg Fosstodon) and P Boy (Fedora Docs team) for helpful feedback.

## wiki

For the spirit of learning by doing, I added a wiki page on this project to write about great text processing utilities such as rg (ripgrep), sed, tr and join.

Work in progress: https://gitlab.com/hanku.lee/quickdocs-metadata-mass-update/-/wikis/Shell-scripting

## License
MIT for software or CC BY-SA for content

https://docs.fedoraproject.org/en-US/legal/fpca/
