#!/bin/bash

# clone all repos using a for loop in a dir somewhere
#git clone https://pagure.io/fedora-docs/quick-docs.git

# use rg/grep to extract metadata to individual files
find . -name "*.adoc" | sort | sed 's|^./||' > files.csv
rg -i '^:category:' -g '*.adoc' --no-heading -H | sed -e 's/:category://' | tr ':' ','|  tr -d ' ' | sort > categories.csv
rg -i '^:tags:' -g '*.adoc' --no-heading -H | sed 's/:tags://' | tr ':' ',' | sed 's/,$//' | tr -d ' ' | sort > tags.csv

# join the file list with categories to get list where files that don't have any categories are also listed
join -a 1 -t ',' -j 1 files.csv categories.csv > all-categories.csv
# join the file list with categories to get list where files that don't have any tags are also listed
join -a 1 -t ',' -j 1 files.csv tags.csv > all-tags.csv
# join the two
join -t ',' -j 1 all-categories.csv all-tags.csv > complete-list.csv
